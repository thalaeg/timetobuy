﻿using System;
using Microsoft.EntityFrameworkCore;

namespace TimeToBuy.Domain
{
    public class StoreContext: DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options)
            : base(options)
            { }

        public DbSet<Product> Products { get; set; }
    }
}
