﻿using System.Collections.Generic;
using TimeToBuy.Domain;

namespace TimeToBuy.Features
{
    public class ProductService
    {
        private readonly StoreContext storeContext;

        public ProductService(StoreContext storeContext)
        {
            this.storeContext = storeContext;
        }

        public ProductListModel GetProductList()
        {
            var products = storeContext.Products;

            var model = new ProductListModel();
            foreach (var product in products)
            {
                model.Products.Add(new ProductListModel.ProductListItem
                {
                    Id = product.Id,
                    Description = product.Description,
                    Name = product.Name
                });
            }

            return model;
        }

        public ProductDetailsModel GetDetails(int id)
        {
            var product = storeContext.Products.Find(id);

            return new ProductDetailsModel
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Price = product.Price
            };
        }
    }
}
