import React, { Component } from 'react';
import { Navbar, Container, Nav} from 'react-bootstrap';
import './NavMenu.css';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render () {
    return (
      <header>
        <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3">
            <Container>
                <Navbar.Brand href="/">TimeToBuy</Navbar.Brand>
                <Nav className="">
                    <Nav.Link className="text-dark" href="/">Home</Nav.Link>
                    <Nav.Link className="text-dark" href="/counter">Counter</Nav.Link>
                    <Nav.Link className="text-dark" href="/fetch-data">Fetch data</Nav.Link>
                </Nav>
            </Container>
        </Navbar>
      </header>
    );
  }
}
