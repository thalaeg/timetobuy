﻿import React from 'react';
import { Card } from 'react-bootstrap';

export default function ProductCard(props) {

    return (
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>{props.product.name}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
                <Card.Text>
                    {props.product.description}
                </Card.Text>
                <Card.Link href={`/product/${props.product.id}`}>Card Link</Card.Link>
            </Card.Body>
        </Card>
    )
}