﻿import React, { Component } from 'react';
import { Row,Col,Media } from 'react-bootstrap';
 

export default class ProductDetails extends Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    async componentDidMount() {
        try {
            const response = await fetch(`api/product/${this.props.match.params.id}`);
            if (!response.ok) throw Error(response);
            const json = await response.json();
            this.setState(json);
        } catch (error) {
            console.log(error)
        }
    }


    render() {
        return (
        <Row>
            <Col>
                <Media>
                    <img
                        width={600}
                        height={600}
                        className="mr-3"
                        src="https://via.placeholder.com/600"
                        alt="Product"
                    />
                    <Media.Body>
                            <h1>{this.state.name}</h1>
                            <p>{this.state.description}</p>
                            <p>${this.state.price}</p>                       
                    </Media.Body>
                </Media>
            </Col>
        </Row>
        )
    }
}