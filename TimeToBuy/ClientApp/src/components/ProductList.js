﻿import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import ProductCard from './ProductCard';

export default class ProductList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: []
        }
    }

    async componentDidMount() {
        // make API call
        try {
            const response = await fetch('/api/product');
            if (!response.ok) throw Error(response);
            const json = await response.json();
            this.setState({ products: json.products });
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        return (
            <Row>
                {this.state.products.map(product =>
                    <Col key={product.id} lg="4">
                        <ProductCard product={product} />
                    </Col>
                )}
            </Row>
        );
    }
}
